const DIRECTIONS = {
    up: {x: 0, y: 1},
    right: {x: 1, y: 0},
    down: {x: 0, y: -1},
    left: {x: -1, y: 0},
    get byButtonCode() {
        return {
            37: this.left,
            38: this.up,
            39: this.right,
            40: this.down,
        }
    },
    fromButtonCode(code) {
        return this.byButtonCode[code];
    }
};


function getSquareKey(x, y) {
    return `${x}|${y}`;
}


class Square {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    get htmlObject() {
        return $(`.square[x=${this.x}][y=${this.y}]`);
    }
}


class Squares {
    constructor(maxX, maxY) {
        this.empty = {};
        this.snake = {};
        this.food = {};
        this.trap = {};
        this.all = {};
        for (let i = 1; i <= maxX; i++) {
            for (let j = 1; j <= maxY; j++) {
                let squareKey = getSquareKey(i, j);
                this.empty[squareKey] = new Square(i, j);
                this.all[squareKey] = "empty";
            }
        }
    }

    change(squareKey, before, after) {
        let square = this[before][squareKey];
        delete this[before][squareKey];
        this[after][squareKey] = square;
        this.all[squareKey] = after;
        square.htmlObject.removeClass(before);
        square.htmlObject.addClass(after);
    }

    insert(after) {
        let emptySquaresKeys = Object.keys(this.empty);
        let index = Math.floor(Math.random() * emptySquaresKeys.length);
        let squareKey = emptySquaresKeys.splice(index, 1)[0];
        this.change(squareKey, "empty", after);
        return squareKey;
    }

    insertTrap(chance) {
        if (Math.random() < chance) {
            let squareKey = allSquares.insert("trap");
            setTimeout(() => {this.change(squareKey, "trap", "empty")}, 10000);
        }
    }
}


class Snake {
    constructor(taleSquaresData, headSquareData, direction) {
        this.squaresKeys = [];
        for (let squareData of taleSquaresData) {
            this.occupySquare(getSquareKey(...squareData), "empty");
        }
        let headSquareKey = getSquareKey(...headSquareData);
        this.headSquareKey = headSquareKey;
        this.occupySquare(headSquareKey, "empty");
        this.direction = direction;
        this.lastDirection = direction;
    }

    move() {
        let gotFood = false;
        let headSquare = allSquares.snake[this.headSquareKey];
        let newHeadSquareKey = getSquareKey(headSquare.x + this.direction.x, headSquare.y + this.direction.y);
        let currentNewHeadSquarePlace = allSquares.all[newHeadSquareKey];

        if (!currentNewHeadSquarePlace || ["snake", "trap"].includes(currentNewHeadSquarePlace)) {
            clearInterval(gameRoller);
            $(".snake").addClass("broken");
            return;
        }
        else if (currentNewHeadSquarePlace === "food") {
            gotFood = true;
        }
        else {
            this.dropTail();
        }

        this.headSquareKey = newHeadSquareKey;
        this.occupySquare(newHeadSquareKey, currentNewHeadSquarePlace);
        this.lastDirection = this.direction;
        if (gotFood) {
            allSquares.insert("food");
            allSquares.insertTrap(1);
        }

    }

    occupySquare(squareKey, before) {
        allSquares.change(squareKey, before, "snake");
        this.squaresKeys.push(squareKey);
    }

    dropTail() {
        allSquares.change(this.squaresKeys.shift(), "snake", "empty");
    }

    getNewDirection(direction) {
        if (direction.x + this.lastDirection.x != 0 || direction.y + this.lastDirection.y != 0) {
            this.direction = direction;
        }
    }
}


var allSquares = new Squares(10, 10);
var snake = new Snake([[2, 5], [3, 5]], [4, 5], DIRECTIONS.right);
var gameRoller = setInterval(() => {snake.move()}, 1000);
// var points = 0;


allSquares.insert("food");


$(window)
.on('keydown', function(e) {
    newDirection = DIRECTIONS.fromButtonCode(e.which);
    if (newDirection) {
        snake.getNewDirection(newDirection);
    }
});
